﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.Sei_la
{
    class DataBaseDbZ
    {
        public int salvar (DTODbZ DBZSalvar)
        {

            string script =
                @"INSERT INTO tb_produto(nm_produto, vl_preco)
                  VALUES(@nm_produto, @vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", DBZSalvar.Nome));
            parms.Add(new MySqlParameter("vl_preco", DBZSalvar.Valor));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;


        }
        public void alterar(DTODbZ DBZalterar)
        {
            string script =
                @"UPDATE tb_produto SET  (
                  nm_produto = @nm_produto,
                  vl_preco = @vl_preco,
                  WHERE id_produto = @id_produto";

            List <MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", DBZalterar.Nome));
            parms.Add(new MySqlParameter("vl_preco", DBZalterar.Valor));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void remover (int Id)
        {
            string script =
                @"DELETE FROM tb_produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_curso", Id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

            
        }

        public List <DTODbZ> listar()
        {
            string script = @"SELECT *FROM tb_produto";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);


            List <DTODbZ> lojinhA = new List<DTODbZ>();
            while (reader.Read())
            {
                DTODbZ produto = new DTODbZ();
                produto.Id = reader.GetInt32("id_produto");
                produto.Nome = reader.GetString("nm_produto");
                produto.Valor = reader.GetDecimal("vl_preco");

                lojinhA.Add(produto);
                
                 
            }
            reader.Close();

            return lojinhA;

        }
             

    }
}
